export default class BarberShopHelper {

    static unformatService(service) {
        const time = service.minutes.split(":");
        const minutes = parseInt(time[0] * 60) + parseInt(time[1]);
        const price = service.price.replace(',','.');
        return Object.assign({}, service, {minutes, price});
    }

    static formatService(service) {
        let price = '' + service['price'];
        price = price.replace('.',',');
        const priceSplit = price.split(',');
        const thereAreCents = priceSplit.length !== 1;
        if (thereAreCents) {
            let cents = priceSplit[1];
            if (cents.length === 1) price += '0';
        } else {
            price += ',00';
        }
        let minutes = service['minutes'];
        let hours = '' + parseInt(minutes / 60);
        if (hours.length === 1) hours = '0' + hours;
        minutes = '' + (minutes - (hours * 60));
        if (minutes.length === 1) minutes = '0' + minutes;
        minutes = hours + ':' + minutes;
        return Object.assign({}, service, {price, minutes});
    }

    static generateEmptyService() {
        return {id: '', description: '', price: '', minutes: ''};
    }
}
