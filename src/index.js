import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route,Switch, Redirect} from 'react-router-dom';
import App from './App';
import Index from './component/Index'
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import './css/reset.css';
import './css/global.css';
import {userModal} from './reducer/userModal';
import {barberShop} from './reducer/barberShop';
import {barberShopServices} from './reducer/barberShopServices';

function isLoggedIn() {
    return localStorage.getItem('auth-token') != null;
}

const reducers = combineReducers({userModal, barberShop, barberShopServices});
const store = createStore(reducers, applyMiddleware(thunkMiddleware));

ReactDOM.render(
    (<Router>
        <Switch>
            <Provider store={store}>
                <Route exact path="/" render={() => (isLoggedIn() ? <Redirect to="/barbershop"/> : <Index/>)}/>
                <Route exact path="/barbershop" render={() => (!isLoggedIn() ? <Redirect to="/"/> : <App/>)}/>
            </Provider>
        </Switch>
    </Router>),
     document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
