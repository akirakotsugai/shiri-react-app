export default class ActionCreator {

    static newModalContent(contentType) {
        return {type: 'NEW_MODAL_CONTENT', content: contentType};
    }

    static newBarberShop(barberShop) {
        return {type: 'NEW_BARBERSHOP', barberShop};
    }

    static newBarberShopContent(barberShopContent) {
        return {type: 'NEW_BARBERSHOP_CONTENT', barberShopContent};
    }

    static showServiceForm(show) {
        return {type: 'SHOW_SERVICE_FORM', show};
    }

    static updateBarberShopServices(services) {
        return {type: 'UPDATE_BARBERSHOP_SERVICES', services};
    }

    static addOrUpdateBarberShopService(service) {
        return {type: 'ADD_OR_UPDATE_BARBERSHOP_SERVICE', service};
    }

    static removeBarberShopService(serviceId) {
        return {type: 'REMOVE_BARBERSHOP_SERVICE', serviceId};
    }

    static updateServiceForm(changedProperties) {
        return {type: 'UPDATE_SERVICE_FORM', changedProperties};
    }


}
