export function barberShop(state='', action) {

    if (action.type === 'NEW_BARBERSHOP') {
        return action.barberShop;
    }

    if (action.type === 'NEW_BARBERSHOP_CONTENT') {
        return Object.assign({}, state, {content: action.barberShopContent});
    }

    return state;

}
