import BarberShopHelper from '../helper/BarberShopHelper';

export function barberShopServices(
  state={services: [], showForm: false, form: BarberShopHelper.generateEmptyService()}, action) {

    if (action.type === 'SHOW_SERVICE_FORM') {
        return Object.assign({}, state, {showForm: action.show});
    }

    if (action.type === 'UPDATE_BARBERSHOP_SERVICES') {
        return Object.assign({}, state, {services: action.services});
    }

    if (action.type === 'ADD_OR_UPDATE_BARBERSHOP_SERVICE') {
        let services = state.services.filter(service => service.id !== action.service.id);
        services.push(action.service);
        return Object.assign({}, state, {services});
    }

    if (action.type === 'REMOVE_BARBERSHOP_SERVICE') {
        const services = state.services.filter(service => service.id !== action.serviceId);
        return Object.assign({}, state, {services});
    }

    if (action.type === 'UPDATE_SERVICE_FORM') {
        const newForm = Object.assign({}, state.form, action.changedProperties);
        return Object.assign({}, state, {form: newForm});
    }

    return state;

}
