export function userModal(state='', action) {
    if (action.type === 'NEW_MODAL_CONTENT') {
        return action.content;
    }
    return state;
}
