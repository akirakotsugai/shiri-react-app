import ActionCreator from '../action/ActionCreator';
import BarberShopHelper from '../helper/BarberShopHelper';

export default class BarberShopApi {

    static getInfo() {
        return dispatch => {
            fetch(`http://localhost:8888/ShirimeBarber/barbershop/?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`)
            .then(response => response.json())
            .then(barberShop => {
                dispatch(ActionCreator.newBarberShop(barberShop));
                return barberShop;
            });
        }
    }

    static getServices(barberShopId) {
        return dispatch => {
            fetch(`http://localhost:8888/ShirimeBarber/barbershop/services/?barberShopId=${barberShopId}&X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`)
            .then(response => response.json())
            .then(services => {
                dispatch(ActionCreator.updateBarberShopServices(services));
                return services;
            });
        }
  }

    static saveService(service) {
        return dispatch => {
            const requestInfo = {
              method: 'POST',
              body: JSON.stringify(BarberShopHelper.unformatService(service)),
              headers: new Headers({
                'Content-type': 'application/json'
              })
            };
            fetch(`http://localhost:8888/ShirimeBarber/barbershop/service/?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, requestInfo)
            .then(response => response.json())
            .then(service => {
                dispatch(ActionCreator.addOrUpdateBarberShopService(service));
                dispatch(ActionCreator.updateServiceForm(BarberShopHelper.generateEmptyService()));
                return service;
            });
        }
    }

    static removeService(serviceId) {
        return dispatch => {
            fetch(`http://localhost:8888/ShirimeBarber/barbershop/service/?serviceId=${serviceId}&X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, {method: 'DELETE'})
            .then(response => {
                if (response.ok) {
                    dispatch(ActionCreator.removeBarberShopService(serviceId));
                    return serviceId;
                }
            });
        }
    }

}
