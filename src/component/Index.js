import React, {Component} from 'react';
import style from '../css/index.module.css';
import UserModal from '../component/UserModal';


export default class Index extends Component {

    render() {
        return (
          <div>
              <header id={style.indexHeader}>
                  <h1 id={style.indexTitle}>Shirime</h1>
                  <UserModal store={this.props.store}/>
                  <p id={style.indexSubtitle}>Encontre e agende serviços em barbearias.</p>
              </header>
          </div>
        );
    }
}
