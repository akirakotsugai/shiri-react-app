import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import FormField from './FormField';
import FormButton from './FormButton';
import formStyle from '../css/shirime-form.module.css';
import modalStyle from '../css/user-modal.module.css';

class Login extends Component {

    constructor() {
        super();
        this.state = {
            login: '',
            password: '',
            authenticationError: ''
        };
    }

    updateStateWithFieldValue(stateField, event) {
        let json = {};
        json[stateField] = event.target.value;
        this.setState(json);
    }

    authenticate(event) {
        event.preventDefault();
        const requestInfo = {
            method: 'POST',
            body: JSON.stringify({login: this.state.login, password: this.state.password}),
            headers: new Headers({
                'Content-type': 'application/json'
            })
        };
        fetch('http://localhost:8888/ShirimeBarber/user/authenticate', requestInfo)
        .then(response => {
            if (response.ok) {
                return response.text();
            } else {
                throw new Error('não foi possível efetuar o login');
            }
        })
        .then(token => {
            localStorage.setItem('auth-token', token);
            this.props.history.push('barbershop');
        })
        .catch(erro => this.setState({authenticationError: erro.message}));
    }

    render() {
        return (
            <form id="user-form">
                <div id="user-signin" className={formStyle.formColumn}>
                    <FormField label="Login" id="login-login" name="login" title="Somente letras são permitidos, com um mínimo de 5 dígitos" callback={this.updateStateWithFieldValue.bind(this, 'login')}/>
                    <FormField label="Senha" id="login-password" name="password" type="password" callback={this.updateStateWithFieldValue.bind(this, 'password')}/>
                    <br/>
                    <span id={modalStyle.authenticationError}>{this.state.authenticationError}</span>
                    <br/>
                    <a id="signup-link" className={modalStyle.signupLink} onClick={this.props.callback} href="/" >Ainda não tenho uma conta.</a>
                </div>
                <FormButton id="btn-login" text="Entrar" onClick={this.authenticate.bind(this)}/>
            </form>
        );
    }

}

export default withRouter(Login);
