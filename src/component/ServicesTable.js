import React, {Component} from 'react';
import ServiceTr from './ServiceTr';
import style from '../css/barbershop-services.module.css';

export default class ServicesTable extends Component {

    render() {
        return (
            <div id={style.servicesTableDiv} className={this.props.showForm ? style.floatRight : ''}>
                <h3>Serviços</h3>
                <table id={style.servicesTable}>
                    <thead>
                        <tr>
                            <th>Serviço</th>
                            <td id={style.priceTitle}>Preço</td>

                            <td id={style.timeLengthTitle}>Tempo</td>
                            <td><button id="btn-add-service" className={style.btnAddService} onClick={this.props.callbackUI}>+</button></td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.services.map(service => (<ServiceTr key={service.id} service={service} removeCallback={this.props.removeCallback} editingCallback={this.props.editingCallback}/>))}
                    </tbody>
                </table>
            </div>
        );
    }
}
