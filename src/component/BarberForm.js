import React, {Component} from 'react';
import PersonalInfo from './PersonalInfo';
import BarberShopInfo from './BarberShopInfo';
import FormButton from './FormButton';
import modalStyle from '../css/user-modal.module.css';

export default class BarberForm extends Component {

    render() {

        return (
            <form id={modalStyle.barberForm}>
                <PersonalInfo/>
                <BarberShopInfo/>
                <br/>
                <FormButton id="btn-barber-form-reg" text="Cadastrar"/>
            </form>
        );
    }

}
