import React, {Component} from 'react';
import FormField from './FormField';
import style from '../css/shirime-form.module.css';

export default class PersonalInfo extends Component {

    render() {
        return (
            <div id="personal-info" className={style.formColumn}>
                <FormField label="Login" id="login-registration" name="login" title="Somente letras são permitidos, com um mínimo de 5 dígitos" type="text"/>
                <FormField label="Senha" id="password-registration" name="password" type="password"/>
                <FormField label="Senha Novamente" id="password-again-registration" name="password-again" type="password"/>
                <FormField label="Nome" id="name-registration" name="name" type="text"/>
                <FormField label="Sobrenome" id="surname-registration" name="surname" type="text"/>
                <FormField label="Nascimento" id="date-of-birth-registration" name="dateOfBirth" type="text"/>
                <FormField label="Email" id="email-registration" name="email" type="text"/>
                <FormField label="CPF" id="cpf-registration" name="cpf" type="text"/>
            </div>
        );
    }
}
