import React, {Component} from 'react';
import style from '../css/shirime-form.module.css';

export default class FormButton extends Component {

    render() {
        return (
            <button id={this.props.id} className={style.btnShirime} onClick={this.props.onClick}>{this.props.text}</button>
        );
    }

}
