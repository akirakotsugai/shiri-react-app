import React, {Component} from 'react';
import PersonalInfo from './PersonalInfo';
import FormButton from './FormButton';

export default class CustomerForm extends Component {

    render() {
        return (
            <form>
                <PersonalInfo/>
                <FormButton id="btn-register" text="Cadastrar"/>
            </form>
        );
    }

}
