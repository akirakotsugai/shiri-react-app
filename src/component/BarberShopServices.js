import React, {Component} from 'react';
import {connect} from 'react-redux';
import ServiceForm from './ServiceForm';
import ServicesTable from './ServicesTable';
import ActionCreator from '../action/ActionCreator';
import BarberShopApi from '../api/BarberShopApi';
import BarberShopHelper from '../helper/BarberShopHelper';

class BarberShopServices extends Component {

    componentWillMount() {
        this.props.updateServices(this.props.barberShopInfo.id);
    }

    toggleForm(event) {
        event.preventDefault();
        this.props.showServiceForm(event.target.id === 'btn-add-service');
    }

    fillInServiceForm(service, event) {
        delete service['formattedPrice'];
        delete service['formattedTimeLength'];
        this.props.showServiceForm(true);
        this.props.updateForm(BarberShopHelper.formatService(service));
    }

    updateFormFieldValue(stateField, event) {
        let json = {};
        json[stateField] = event.target.value;
        this.props.updateForm(json);
    }

    saveService(event) {
        event.preventDefault();
        this.props.saveService(this.props.servicesInfo.form);
    }

    removeService(serviceId, event) {
        event.preventDefault();
        this.props.removeService(serviceId);
    }

    render() {
        return (
            <div>
                <ServiceForm form={this.props.servicesInfo.form} callbackUI={this.toggleForm.bind(this)} callbackSubmit={this.saveService.bind(this)} updateState={this.updateFormFieldValue.bind(this)} showForm={this.props.servicesInfo.showForm}/>
                <ServicesTable services={this.props.servicesInfo.services} callbackUI={this.toggleForm.bind(this)} removeCallback={this.removeService.bind(this)} editingCallback={this.fillInServiceForm.bind(this)} showForm={this.props.servicesInfo.showForm}/>
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        servicesInfo: state.barberShopServices,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        showServiceForm: (show) => {
            dispatch(ActionCreator.showServiceForm(show));
            if (!show) {
                dispatch(ActionCreator.updateServiceForm(BarberShopHelper.generateEmptyService()));
            }
        },
        updateServices: (barberShopId) => {
            dispatch(BarberShopApi.getServices(barberShopId));
        },
        saveService: (service) => {
            dispatch(BarberShopApi.saveService(service));
        },
        removeService: (serviceId) => {
            dispatch(BarberShopApi.removeService(serviceId));
        },
        updateForm: (changedProperty) => {
            dispatch(ActionCreator.updateServiceForm(changedProperty));
        }
    }
}

const BarberShopServicesContainer = connect(mapStateToProps, mapDispatchToProps)(BarberShopServices);
export default BarberShopServicesContainer;
