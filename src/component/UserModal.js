import React, {Component} from 'react';
import Modal from 'react-modal';
import Login from './Login';
import UserTypeOptions from './UserTypeOptions';
import CustomerForm from './CustomerForm';
import BarberForm from './BarberForm';
import FormButton from './FormButton';
import ActionCreator from '../action/ActionCreator';
import '../css/reset.css';
import style from '../css/user-modal.module.css';
import {connect} from 'react-redux';

Modal.setAppElement('#root');

const userModalContent = {
    login : 'login',
    userTypeOptions : 'userTypeOptions',
    customerForm : 'customerForm',
    barberForm : 'barberForm'
}

class UserModal extends Component {

    constructor() {
        super();
        this.state = {
            modalIsOpen: false
        };
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
        this.props.changeContent(userModalContent.login);
    }

    afterOpenModal() {

    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    modalTitle() {
        let title;
        switch (this.props.content) {
            case userModalContent.login: {
                title = 'Opa';
                break;
            }
            case userModalContent.userTypeOptions: {
                title = 'Quem é você?';
                break;
            }
            case userModalContent.customerForm: {
                title = 'Cliente';
                break;
            }
            case userModalContent.barberForm: {
                title = 'Barbeiro';
                break;
            }
            default: {
                break;
            }
        }
        return title;
    }

    modalContent() {
        if (this.props.content === userModalContent.login) {
            return (
                <Login callback={this.changeContent.bind(this)}/>
            );
        } else if (this.props.content === userModalContent.userTypeOptions) {
            return (
                <UserTypeOptions callback={this.changeContent.bind(this)}/>
            );
        } else if (this.props.content === userModalContent.customerForm) {
            return (
                <CustomerForm/>
            );
        } else if (this.props.content === userModalContent.barberForm) {
            return (
                <BarberForm/>
            );
        }
    }

    changeContent(event) {
        event.preventDefault();
        event.persist();
        let content;
        if (event.target.id === 'signup-link') {
            content = userModalContent.userTypeOptions;
        } else if (event.target.id === 'i-am-a-client') {
            content = userModalContent.customerForm;
        } else if (event.target.id === 'i-am-a-barber') {
            content = userModalContent.barberForm;
        }
        this.props.changeContent(content);
    }

    render() {
        return (
            <div>
                <FormButton id={style.btnSignIn} text="Entrar" onClick={this.openModal}/>
                <Modal
                  isOpen={this.state.modalIsOpen}
                  onAfterOpen={this.afterOpenModal}
                  onRequestClose={this.closeModal}
                  id="user-modal"
                  className={style.modal}
                >
                    {/* Modal window */}
                    <div className={style.modalWindow}>
                        {/* Modal header */}
                        <div className={style.modalHeader}>
                            <span className={style.modalClose} onClick={this.closeModal}>&times;</span>
                            <h3 id={style.modalTitle}>{this.modalTitle()}</h3>
                        </div>
                        {/* Modal content */}
                        <div className={style.modalContent}>
                            {this.modalContent()}
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {content: state.userModal}
}

const mapDispatchToProps = dispatch => {
    return {
        changeContent: content => {
            dispatch(ActionCreator.newModalContent(content));
        }
    }
}

const UserModalContainer = connect(mapStateToProps, mapDispatchToProps)(UserModal);
export default UserModalContainer;
