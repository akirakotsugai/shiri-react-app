import React, {Component} from 'react';
import FormField from './FormField';
import FormButton from './FormButton';
import serviceStyle from '../css/barbershop-services.module.css';

export default class ServiceForm extends Component {

    render() {
        return (
            <div id={serviceStyle.serviceRegistration} className={this.props.showForm ? serviceStyle.showAndFloatLeft : serviceStyle.hide}>
                <h3 id="service-registration-title">Novo</h3>
                <form>
                    <FormField label="Serviço" id="service-registration-description" name="description" type="text" value={this.props.form.description} callback={this.props.updateState.bind(this, 'description')}/>
                    <FormField label="Preço" id="service-registration-price" name="price" type="text" value={this.props.form.price} callback={this.props.updateState.bind(this, 'price')}/>
                    <FormField label="Tempo HR:MIN" id="service-registration-time-length" name="minutes" value={this.props.form.minutes} type="text" callback={this.props.updateState.bind(this, 'minutes')}/>
                    <div className={serviceStyle.saveCancel}>
                        <FormButton id={serviceStyle.btnCancelServiceRegistration} onClick={this.props.callbackUI} text="cancelar"/>
                        <FormButton id={serviceStyle.btnSaveServiceRegistration} onClick={this.props.callbackSubmit} text="salvar"/>
                    </div>
                </form>
            </div>
        );
    }

}
