import React, {Component} from 'react';
import style from '../css/user-modal.module.css';

export default class UserTypeOptions extends Component {

    render() {
        return (
            <div id={style.chooseUserType}>
                <ul>
                    <li>
                        <button type="button" id="i-am-a-barber" onClick={this.props.callback}>
                        Sou barbeiro
                        </button>
                    </li>
                    <li>
                        <button type="button" id="i-am-a-client" onClick={this.props.callback}>
                        Sou cliente
                        </button>
                    </li>
                </ul>
            </div>
        );
    }

}
