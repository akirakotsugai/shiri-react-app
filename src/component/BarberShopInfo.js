import React, {Component} from 'react';
import FormField from './FormField';
import style from '../css/shirime-form.module.css';

export default class BarberShopInfo extends Component {

    render() {
        return (
            <div id="barberShop-info" className={style.formColumn}>
                <FormField label="Nome da Barbearia" id="barbershop-name-registration" name="barberShop.name" type="text"/>
                <FormField label="CNPJ" id="cnpj-registration" name="barberShop.cnpj" type="text"/>
                <FormField label="CEP" id="cep-registration" name="barberShop.address.zipCode" type="text"/>
                <FormField label="Estado" id="state-registration" className="address-auto-completable" name="barberShop.address.state" type="text"/>
                <FormField label="Cidade" id="city-registration" className="address-auto-completable" name="barberShop.address.city" type="text"/>
                <FormField label="Bairro" id="district-registration" className="address-auto-completable" name="barberShop.address.district" type="text"/>
                <FormField label="Endereço" id="address-registration" className="address-auto-completable" name="barberShop.address.street" type="text"/>
                <FormField label="Número" id="number-registration" name="barberShop.address.number" type="text"/>
            </div>
        );
    }
}
