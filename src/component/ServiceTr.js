import React, {Component} from 'react';
import style from '../css/barbershop-services.module.css';

export default class ServiceTr extends Component {

    render() {
        return (
            <tr>
                <th>{this.props.service.description}</th>
                <td>{this.props.service.formattedPrice}</td>
                <td>{this.props.service.formattedTimeLength}</td>
                <td><button className={style.btnRemoveService} onClick={this.props.removeCallback.bind(this, this.props.service.id)}>-</button></td>
                <td><button className={style.btnEditService} onClick={this.props.editingCallback.bind(this, Object.assign({}, this.props.service))}>✎</button></td>
            </tr>
        );
    }

}
