import React, {Component} from 'react';
import style from '../css/shirime-form.module.css';

export default class FormField extends Component {

    render() {
        return (
            <div>
                <label className={style.formLabel} htmlFor={this.props.id}>{this.props.label}</label>
                <input id={this.props.id} className={style.formText + ' ' + this.props.class} name={this.props.name} title={this.props.title} value={this.props.value} type={this.props.type} onChange={this.props.callback}/>
            </div>
        );
    }

}
