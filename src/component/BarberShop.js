import React, {Component} from 'react';
import style from '../css/barbershop-base.module.css';
import {connect} from 'react-redux';
import TimeTable from './BarberShopTimeTable.js';
import Services from './BarberShopServices.js';
import Profile from './BarberShopProfile.js';
import Home from './BarberShopHome.js';
import BarberShopApi from '../api/BarberShopApi';
import ActionCreator from '../action/ActionCreator';

const barberShopContent = {
    home: "home",
    profile: "profile",
    timetable: "timetable",
    services: "services"
};

class BarberShop extends Component {

    componentWillMount() {
        if (this.props.barberShopInfo === '') {
            this.props.updateBarberShopInfo();
        }
    }

    changeContent(event) {
        event.preventDefault();
        this.props.updateBarberShopContent(event.target.id);
    }

    barberShopContent(content) {
        switch(content) {
            case barberShopContent.home: {
                return (<Home barberShopInfo={this.props.barberShopInfo}/>);
            }
            case barberShopContent.profile: {
                return (<Profile barberShopInfo={this.props.barberShopInfo}/>);
            }
            case barberShopContent.timetable: {
                return (<TimeTable barberShopInfo={this.props.barberShopInfo}/>);
            }
            case barberShopContent.services: {
                return (<Services barberShopInfo={this.props.barberShopInfo}/>);
            }
            default: {
                return (<Home barberShopInfo={this.props.barberShopInfo}/>);
            }
        }
    }

    render() {
        return (
            <div id={style.barbershopBody}>
                <header id={style.barbershopHeader}>
                  <div id={style.shirimeLogo}>
                      <h1>Shirime<br/>Barber</h1>
                  </div>
                  <div id={style.titleBarbershop}>
                      <h2>{this.props.barberShopInfo.name}</h2>
                  </div>
                  <nav id={style.barbershopNav}>
                    <ul>
                      <li><a href="/barbershop" onClick={this.changeContent.bind(this)}><span id="home">Início</span></a></li>
                      <li><a href="/barbershop" onClick={this.changeContent.bind(this)}><span id="profile">Perfil</span></a></li>
                      <li><a href="/barbershop" onClick={this.changeContent.bind(this)}><span id="timetable">Horários</span></a></li>
                      <li><a href="/barbershop" onClick={this.changeContent.bind(this)}><span id="services">Serviços</span></a></li>
                    </ul>
                  </nav>
                </header>
                <main id={style.barbershopMain}>
                    <div id={style.barbershopContent}>
                        {this.barberShopContent(this.props.barberShopInfo.content)}
                    </div>
                </main>
            </div>

        );
    }
}

const mapStateToProps = state => {
    return {
        barberShopInfo: state.barberShop,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateBarberShopInfo: () => {
            dispatch(BarberShopApi.getInfo());
        },
        updateBarberShopContent: (content) => {
            dispatch(ActionCreator.newBarberShopContent(content));
        }
    }
}

const BarberShopContainer = connect(mapStateToProps, mapDispatchToProps)(BarberShop);
export default BarberShopContainer;
